@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente : Desconecta do Servidor.
Cliente -> Cliente: Pega o Id do Jogador.
Cliente -> Cliente: Gera o comando de envio: playerleave  < iDJogador > 
Cliente -> Cliente: Converte a mensagem para Hexadecimal.
Cliente -> Servidor : Envia mensagem para o servidor, usando o Protocolo TCP.
Servidor -> Servidor: Recebe o comando e deixa de receber comandos desse cliente que desconectou do servidor.
Servidor -> Cliente : Envia mensagem para todos os clientes usando a resposta: playerleave  < iDJogador > em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Jogador: Exibe a confirma��o de desconex�o no bate papo.
Cliente -> Cliente: Para de trocar mensagens com o Servidor
@enduml
