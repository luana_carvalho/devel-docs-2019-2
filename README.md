﻿
# Equipe da Documentação do Desenvolvedor

View docs: [Docs Tetrinet](https://gitlab.com/tetrinetjs/devel-docs/tree/jonathas/Pages)

----
## Objetivos:

- Realizar a documentação do projeto TetrinetJS.

## Grupo 3 - Membros

- Luana Silva Carvalho
- Debora dos Santos e Silva
- Carine Sá de Morais Aquino
- Gustavo Alves de Souza


## Tarefas do Sprint 1 (02/09/19 - 08/09/19)

- [Criar documentação do server atual](https://gitlab.com/tetrinetjs/server)
- [Revisar histórias de usuários](https://en.wikipedia.org/wiki/User_story)
- [Revisar diagrama de sequência](https://gitlab.com/tetrinetjs/devel-docs/tree/master/Pages)
- [Revisar documentação tretinetProtocol4](https://github.com/xale/iTetrinet/wiki/tetrinet-protocol:-server-to-client-messages)

## Tarefas do Sprint 2 (09/09/19 - 15/09/19)



## Tarefas do Sprint 3 (16/09/19 - 22/09/19)



## Tarefas do Sprint 4 (23/09/19 - 29/09/19)


----

# Equipe 2019/1

## Objetivos:
* Criar as histórias de usuários
    * https://en.wikipedia.org/wiki/User_story
* Modelar o protocolo do jogo tetrinet
* Documentar o projeto do tetrinetjs sobre o ponto de vista do desenvolvedor

## Grupo 8 - membros
* JADER HENRIQUE FARIA AMORIM (110992) (Líder)
* FELIPE GONÇALVES FERREIRA
* JOSUE GOMEZ QUEIROZ
* JONATHAS ASSUNCAO ALVES
* JOSUÉ GOMES QUEIROZ
* ODAIR XAVIER DA SILVA
* RODRIGO BARBOSA TAVARES

## Tarefas da sprint 1 (25/03 a 31/03): 
* Levantar as histórias de usuários
    * Instalar o jogo (cliente e servidor) e realizar testes
* Dividir as histórias de usuários em sprints
* Criar um modelo de diagrama de sequência
    * Sugestão: usar o iplantuml/jupyter
        * https://gitlab.com/marceloakira/tutorial/tree/master/diagramas-de-sequencia
