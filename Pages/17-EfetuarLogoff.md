﻿
#  Requisito 17 - Efetuar Logoff

## 1 - Resumo
**História de Usuário**
>Como usuário, gostaria de sair do jogo, desconectar do servidor.

**Pré-condições**
>Estar conectado com o servidor.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O servidor informa que o jogador foi desconectado;<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. O servidor informa que o jogador ira perder o jogo;<br/>
    2.2. O servidor não retorna uma reposta ao jogador;<br/>

**Observações**
>Ao sair do jogo o usuário vai perder a partida. Assim não vai ganhar o jogo. Caso o usuário desejar entrar em outra partida do jogo vai começar tudo novamente do inicio. No jogo tetrinet so permite no maximo 6 jogadores.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.

>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerleave < iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que desconectou do servidor;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>706c617965726c656176652031a**ff** (Ao final da string adiciona 'ff')<br/>
> --------- em **Texto imprimivel**: playerleave 1<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerleave < iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que desconectou do servidor;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 706c617965726c656176652031a<br/>
> --------- em **Texto imprimivel**: playerleave 1<br/>
## 3 - Diagrama de sequência

![Requisito 17](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2017/Requisito%2017%20-%20Bate%20Papo%20Canal%20Principal.png)



