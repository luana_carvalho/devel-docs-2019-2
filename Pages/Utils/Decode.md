﻿
#  decode();

**Função usada para decodificar uma mensagem.**
>Exemplo de implementação na linguagem Java:

     /**
     * Decodes TetriNET client initialization string
     *
     * @param initString initialization string
     * @return decoded string
     * @throws IllegalArgumentException thrown if the string can't be decoded
     */
    public static String decode(String initString)
    {
        // check the size of the init string
        if (initString.length() % 2 != 0)
        {
            throw new IllegalArgumentException("Invalid initialization string, the length is not even");
        }

        // parse the hex values from the init string
        int[] dec = new int[initString.length() / 2];

        try
        {
            for (int i = 0; i < dec.length; i++)
            {
                dec[i] = Integer.parseInt(initString.substring(i * 2, i * 2 + 2), 16);
            }
        }
        catch (NumberFormatException e)
        {
            throw new IllegalArgumentException("Invalid initialization string, illegal characters found", e);
        }

        // find the hash pattern for a tetrinet client
        String pattern = findHashPattern(dec, false);

        // find the hash pattern for a tetrifast client
        if (pattern.length() == 0)
        {
            pattern = findHashPattern(dec, true);
        }

        // check the size of the pattern found
        if (pattern.length() == 0)
        {
            throw new IllegalArgumentException("Invalid initialization string, unable to find the pattern");
        }

        // decode the string
        StringBuffer s = new StringBuffer();

        for (int i = 1; i < dec.length; i++)
        {
            s.append((char) (((dec[i] ^ pattern.charAt((i - 1) % pattern.length())) + 255 - dec[i - 1]) % 255));
        }

        return s.toString().replace((char) 0, (char) 255);
    }

**Função auxiliar do Decode().**
>Exemplo de implementação na linguagem Java:

     private static String findHashPattern(int[] dec, boolean tetrifast)
    {
        // the first characters from the decoded string
        char[] data = (tetrifast ? "tetrifaste" : "tetrisstar").toCharArray();

        // compute the full hash
        int[] hash = new int[data.length];

        for (int i = 0; i < data.length; i++)
        {
            hash[i] = ((data[i] + dec[i]) % 255) ^ dec[i + 1];
        }

        // find the length of the hash
        int length = 5;

        for (int i = 5; i == length && i > 0; i--)
        {
            for (int j = 0; j < data.length - length; j++)
            {
                if (hash[j] != hash[j + length])
                {
                    length--;
                }
            }
        }

        return new String(hash, 0, length);
    }
